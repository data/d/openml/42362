# OpenML dataset: ELE-2

https://www.openml.org/d/42362

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Electrical-Maintenance data set

This problem consists of four input variables and the available data set is comprised of a representative number of well distributed examples. In this case, the learning methods are expected to obtain a considerable number of rules. Therefore, this problem involves a larger search space (high complexity).

###Attributes
    1. X1 - real [0.5,11.0]
    2. X2 - real [0.15,8.55]
    3. X3 - real [1.64,142.5]
    4. X4 - real [1.0,165.0]
    5. Y - real [64.470001,8546.030273]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42362) of an [OpenML dataset](https://www.openml.org/d/42362). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42362/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42362/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42362/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

